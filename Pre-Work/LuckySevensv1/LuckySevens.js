function play() {
	var startingBet = document.getElementById("initialBet").value;
	var bet = startingBet;
	var betsArray = [];
	while (bet > 0) {
		var dice1 = Math.floor(Math.random() * 6) + 1;
		var dice2 = Math.floor(Math.random() * 6) + 1;
		var diceRoll = dice1 + dice2;
		if(diceRoll == 7) {
		bet += 4
		} else { 
		bet--
        	}		

	var rollCounter = betsArray.length;
	var highestAmount = Math.max.apply(Math, betsArray);
	var highestPosition = betsArray.indexOf(highestAmount);
	var rollsFromHighest = rollCounter - highestPosition;
        betsArray.push(bet);
}

function showResults() {
	document.getElementById("results").style.display = "inline";
	document.getElementById("resultsPlayAgain").innerHTML = "Would you like to play again?";
	document.getElementById("playButton").innerHTML = "Play Again?";
	document.getElementById("resultsBet").innerHTML = "$" + startingBet + ".00";
	document.getElementById("resultsRollCounter").innerHTML = rollCounter;
	document.getElementById("resultsHighestHeld").innerHTML = "$" + highestAmount + ".00";
	document.getElementById("resultsRollsFromHighest").innerHTML = rollsFromHighest;

}

showResults(); 
}